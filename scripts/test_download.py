#!/usr/bin/env python

import sys
import rospy
from aws_slam_toolbox.srv import *


def download_file(bucket, object_name, file_name):
    rospy.wait_for_service('DownloadS3')
    try:
        download = rospy.ServiceProxy('DownloadS3', DownloadS3)
        resp1 = download(bucket, object_name, file_name)
        return resp1.response
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)

def ping_file(bucket, object_name):
    rospy.wait_for_service('PingS3')
    try:
        ping = rospy.ServiceProxy('PingS3', DownloadS3)
        resp1 = ping(bucket, object_name, '')
        return resp1.response
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)


if __name__ == "__main__":
    
   #print(download_file('outputmapping', 'maps/map1.data', 'map1.data'))
   print(ping_file('outputmapping', 'maps/map15.data'))	
