#!/usr/bin/env python

import rospy
import logging
import boto3
from botocore.exceptions import ClientError
from aws_slam_toolbox.srv import DownloadS3, DownloadS3Response


def download_file(bucket, object_name, file_name):
    """Download a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. 
    :return: True if file was downloaded, else False
    """

    # Download the file
    s3_client = boto3.client('s3')
    try:
        s3_client.download_file(bucket, object_name, file_name)
    except ClientError as e:
        print(e)
        return False
    return True
    
def downloadCB(req):    
    return DownloadS3Response(download_file(req.bucket, req.object_name, req.file_name))

def file_exists(mybucket, mykey):
  s3_client = boto3.client('s3')
  try:
    s3_client.head_object(Bucket=mybucket, Key=mykey)
  except ClientError:
    return False
  return True	
  
  

def pingCB(req):    
    return DownloadS3Response(file_exists(req.bucket, req.object_name))

def main():
    rospy.init_node('s3_file_downloader')
    rospy.Service('DownloadS3', DownloadS3, downloadCB)
    rospy.Service('PingS3', DownloadS3, pingCB)
    rospy.loginfo("s3 File Downloader is running!")
    rospy.spin()

   

if __name__ == '__main__':
    main()
