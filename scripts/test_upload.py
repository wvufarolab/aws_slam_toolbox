#!/usr/bin/env python

import sys
import rospy
from aws_slam_toolbox.srv import *


def upload_file(bucket, object_name, file_name):
    rospy.wait_for_service('UploadS3')
    try:
        upload= rospy.ServiceProxy('UploadS3', DownloadS3)
        resp1 = upload(bucket, object_name, file_name)
        return resp1.response
    except rospy.ServiceException as e:
        print("Service call failed: %s"%e)


if __name__ == "__main__":
    
   print(upload_file('outputmapping', 'maps/map10.data', '/home/ubuntu/environment/mapping_ws/src/aws_slam_toolbox/launch/online_sync.launch'))