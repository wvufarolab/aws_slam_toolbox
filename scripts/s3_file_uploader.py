#!/usr/bin/env python

import rospy
import logging
import boto3
from botocore.exceptions import ClientError
from aws_slam_toolbox.srv import DownloadS3, DownloadS3Response
from aws_slam_toolbox.msg import UploadS3

def upload_file(bucket, object_name, file_name):
    """Download a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. 
    :return: True if file was downloaded, else False
    """

    # Download the file
    s3_client = boto3.client('s3')
    try:
        s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        print(e)
        return False
    return True
    
def uploadCB(req):    
    return DownloadS3Response(upload_file(req.bucket, req.object_name, req.file_name))

def uploadCB2(req):    
    upload_file(req.bucket, req.object_name, req.file_name)

def main():
    rospy.init_node('s3_file_uploader')
    rospy.Service('UploadS3', DownloadS3, uploadCB)
    rospy.Subscriber('UploadS3', UploadS3, uploadCB2)
    rospy.loginfo("s3 File Uploader is running!")
    rospy.spin()

   
	

if __name__ == '__main__':
    main()
