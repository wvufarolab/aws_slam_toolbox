## Long-Term, Multi-session mapping on AWS using SLAM_TOOLBOX 

SLAM_TOOLBOX (http://wiki.ros.org/slam_toolbox) is a pose-graph based 2D SLAM (Simultaneous Localization and Mapping) package for mobile robotics equipped with laser range finders.
This repository leverages the ability of SLAM_TOOLBOX of saving pose-graphs to allow multi-session mapping at Amazon Cloud (AWS).
The repository provides a small modification on SLAM_TOOLBOX that allows it to save a map and restart mapping when an event happens, and a set of tools for running it in the cloud.
Once SLAM_TOOLBOX is running on a AWS RoboMaker job, a remote robot can send laser scan data to the cloud, where a 2D map is built.
Mapping is restarted everytime there is a discontinuity in the data flow, which may indicate a different session or a different robot, 
when pose-graphs are saved at Amazon S3 to be used later, for example, composing a global map.
Robots also receive a local grid map and localization information from the cloud.
The AWS arquitecture implemented is shown in the following figure, where the darker nodes are implemented in this respository.

![arquitecture](/images/arquitecture.jpg?raw=true "AWS Arquitecture")

Thus, this repository provides: 1) a node that receives sensing information from a remote robot and creates the topics and TFs required by SLAM_TOOLBOX; 
2) a Map Manager node that controlls when SLAM_TOOLBOX must save a map and start a new map; 3) two services that allow uploading and downloading maps to/from Amazon S3.

This package uses ROS Bridge (http://wiki.ros.org/rosbridge_suite) and ROSlibpy (https://roslibpy.readthedocs.io/en/latest/) for communication between the robot and AWS; and Boto3 (https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) to write and read to/from S3,

#### Running the package 

Sign in to the AWS RoboMaker console at https://console.aws.amazon.com/robomaker/.

---

##### Creating an environment

In the AWS RoboMaker console, expand **Development** on the left and then select **Development environments**.

Select **Create environment**.

Choose a name for your environnment (ex: mapping) and choose the ROS distribution to be Melodic. Chose the default VPC and any subnet. Click **Create**.

In the console, create a workspace folder: 


```
#!bash

mkdir -p mapping_ws/src
    

```
    
Clone SLAM_TOOLBOX repository inside the src folder and change its version to Melodic: 

```
#!bash

cd mapping_ws/src
git clone https://github.com/SteveMacenski/slam_toolbox.git
cd slam_toolbox
git checkout melodic-devel
cd ..
    
```    
    
Clone this repository inside the src folder: 

```
#!bash

git clone https://bitbucket.org/wvufarolab/aws_slam_toolbox.git
    
```

---

##### Building and bundle the application

Go to the workspace folder (`mapping_ws`) and install dependencies:

```
#!bash

cd ..    
rosdep update
rosdep install --from-paths src --ignore-src -r -y
    
```

Build and bundle your application:

```
#!bash

colcon build
colcon bundle
    
```

Create an Amazon S3 bucket for your application source:

```
#!bash

aws s3 mb s3://mapping
    
```

Copy the robot application source bundle to your Amazon S3 bucket:

```
#!bash

aws s3 cp bundle/output.tar s3://mapping/mapping.tar
    
```

Create an Amazon S3 bucket to save the maps:

```
#!bash

aws s3 mb s3://outputmapping
    
```

---

##### Creating the robot application

In the AWS RoboMaker console (https://console.aws.amazon.com/robomaker/), expand **Development** on the left and then select **Robot applications**.

Select **Create robot application**.

Choose a name for your application (ex: mapping) and choose the ROS distribution to be Melodic. Browse s3 for your X86_64 source file (`s3://mapping/mapping.tar`) and select **Create**.

---

##### Running the robot application


In the AWS RoboMaker console, expand **Simulations** on the left and then select **Simulation Jobs**.

Select **Create simulation job**.

Choose the duration and set the ROS version to be Melodic. Choose a role. It is recomended that you create a new role in the first run, what will set the necessary privilegies automatically.

In Networking, a VPC must be chosen. It is also important that you have a security group that allows external communication. To create a security group go to https://console.aws.amazon.com/ec2/ and select security groups on the left. 
You will need to create two Inboud and two Outbound rules. 1) All TCP, source My IP (or the robot IP), 2) All trafic, source Anywhere.  

Mark yes for **Assign public IP** and click **Next**.

Choose your Robot application (gmapping).

The launch package is `aws_slam_toolbox` and the launch file is `aws_slam_toolbox.launch`.

In **Robot application connectivity** set both the Simulation port and the Application port to 8080 and the type to public. Click **Next**.

We have no simulation application. Select **None** and click **Next**.

Select **Create**.

---

##### Interacting with the application

Once the simulation job status is running, you can interact with your application using rviz, rqt or a terminal.

To see if all nodes are running, **Connect** to the Terminal and see the list of nodes:

```
#!bash

rosnode list
    
```

You can use all command-line ROS tools in the terminal (`rostopic`, `rosparam`, `rosmsg`, etc). 

---

#### Localization and Mapping using your cloud application

To map an environemnt using your cloud application, clone the client node in your local linux machine or robot. Inside the `src` folder of your catkin workspace run:

```
#!bash

git clone https://bitbucket.org/wvufarolab/aws_mapping_meta.git

```

You may need to build  and source your workspace:

```
#!bash

cd ..
catkin_make
source devel/setup.bash

```

Run your robot or simulated robot. Launch files for a simulated Husky robot equiped with plannar Lidar can be found in the aws_mapping package. These will require the instalation of a Husky simulator (http://wiki.ros.org/husky_gazebo/Tutorials/Simulating%20Husky).
For example, if you have the Husky simulation istalled, you may want to run:

```
#!bash

roslaunch aws_mapping husky_willow.launch

```

Launch the client:

```
#!bash

roslaunch aws_mapping send_data.launch IP:=host_public_ip

```

The public IP address (`host_public_ip`) is found in the botton of your simulation job page at AWS RoboMaker.

You should be able to see the laser data and the map being contructed using RVIZ at AWS RoboMaker.

---

If you have any questions or suggestions, please e-mail <guilherme.pereira@mail.wvu.edu>

---

#### The developmet of this software was supported by the Amazon Research Award (ARA) program.