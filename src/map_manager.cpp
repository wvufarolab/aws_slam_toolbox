#include <ros/ros.h>
#include "slam_toolbox/toolbox_msgs.hpp"
#include "sensor_msgs/LaserScan.h"
#include <unistd.h>
#include <sys/stat.h>
#include <pwd.h>
#include "aws_slam_toolbox/DownloadS3.h"
#include "aws_slam_toolbox/UploadS3.h"


ros::Duration time_between_scans(0);
ros::Time time_previous_scan;
bool got_first_scan;

void laserCB(const sensor_msgs::LaserScan::ConstPtr& scan)
{
    if (!got_first_scan){
	      //time_previous_scan = scan->header.stamp;
	      time_previous_scan = ros::Time::now();	
	      got_first_scan = true;
    }	
    //time_between_scans =  time_previous_scan-scan->header.stamp;
    //time_previous_scan = scan->header.stamp;
    time_previous_scan = ros::Time::now();		 
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "map_manager");
  ros::NodeHandle nh("~");
  ros::ServiceClient serialize = nh.serviceClient<slam_toolbox_msgs::SerializePoseGraph>("/slam_toolbox/serialize_map");
  ros::Subscriber sub = nh.subscribe("/scan",1, laserCB);
  
  //ros::ServiceClient client = nh.serviceClient<aws_slam_toolbox::DownloadS3>("/UploadS3");
  //aws_slam_toolbox::DownloadS3 srv;
  //srv.request.bucket ="outputmapping";
  
  ros::Publisher pub = nh.advertise<aws_slam_toolbox::UploadS3>("/UploadS3", 1000 );
  aws_slam_toolbox::UploadS3 upload;
  upload.bucket ="outputmapping";
  
  ros::Rate loop_rate(10);
  got_first_scan = false;
  
  
  // get the home folder where the maps will be saved
  const char *homedir = getenv("HOME");
  if ( homedir == NULL ) {
    homedir = getpwuid(getuid())->pw_dir;
  }
  std::string home_dir(homedir);

  //char buff[FILENAME_MAX]; //create string buffer to hold the current folder path
  //getcwd( buff, FILENAME_MAX );
  //std::string current_working_dir(buff); // current folder path /home/robomaker/.ros/map0.posegraph 
  std::string map_name, object_name;

  int cont = 0;
  struct stat buffer;
  
  while (ros::ok()){

    if (got_first_scan){
       time_between_scans =  ros::Time::now()-time_previous_scan;	
       //ROS_INFO(" Time between scans: %d", abs((int)time_between_scans.toSec()));
       if (abs(time_between_scans.toSec()) > 5){
	  got_first_scan = false; 
	  do {
            //map_name = current_working_dir+"/map"+std::to_string(cont);
            map_name = home_dir+"/map"+std::to_string(cont);
            object_name = "maps/map"+std::to_string(cont);
	    cont++;	 
          }
  	  while (stat ((map_name+".data").c_str(), &buffer) == 0);        	
     
  	  ROS_INFO(" Saving %s ...", map_name.c_str());

  	  slam_toolbox_msgs::SerializePoseGraph msg1;
  	  msg1.request.filename = map_name;
  	  if (!serialize.call(msg1))
  	  {
    		ROS_WARN("SlamToolbox: Failed to serialize pose graph to file, is service running?");
  	  }
      	  else
  	  {
    		ROS_INFO("SlamToolbox: Map serialized - Sending to S3!");
    		std::string file_name;
    		file_name = map_name+".posegraph";
   		//srv.request.file_name = file_name;
		//srv.request.object_name = object_name+".posegraph";
		upload.file_name = file_name;
		upload.object_name = object_name+".posegraph";
        	ROS_INFO(" File name %s   Object name %s", file_name.c_str(), upload.object_name.c_str());
        	//client.call(srv);
		pub.publish(upload);
    		file_name = map_name+".data";
    		//srv.request.file_name = file_name;
        	//srv.request.object_name = object_name+".data";
		upload.file_name = file_name;
		upload.object_name = object_name+".data";
        	ROS_INFO(" File name %s   Object name %s", file_name.c_str(), upload.object_name.c_str());
        	//client.call(srv);
		pub.publish(upload);
  	  }
       }//if 	  
    }// if got_first_scan
     
       
    ros::spinOnce();
    loop_rate.sleep();
  } // while

  return 0;
	

}
